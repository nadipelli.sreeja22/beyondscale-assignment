const config = {
    development: {
        server: {
            micro_server_url: "https://6011bce591905e0017be5755.mockapi.io/"
        }
    },
    production: {
        server: {
            micro_server_url: "https://6011bce591905e0017be5755.mockapi.io/"
        }
    }
}
export default config;