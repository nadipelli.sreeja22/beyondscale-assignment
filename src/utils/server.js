import axios from "axios";
import configFile from './config'
var env = process.env.NODE_ENV || "development";
let config = configFile[env];
export default {

  execute: async (url, method, body) => {
    console.log("requestObj::::::", url);
    let baseURL = config.server.micro_server_url;
    
    return axios({
      url: url,
      method: method,
      headers: {"Content-Type": "application/json"},
      baseURL: baseURL,
      data: body
    });
  }
};