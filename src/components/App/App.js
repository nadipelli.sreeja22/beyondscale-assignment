import './App.css';
import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import { Row, Col, Button, Table, Popconfirm, Modal, Input, message } from "antd";
import applicationApi from '../../services/application-api';

function App() {

  const [userData, setUserData] = useState([]);
  const [isModalVisible, setModelVisibility] = useState(false);
  const [newUser, setNewUser] = useState({});

  // get all users 
  async function getAllUsers() {
    try {
      const apiData = await applicationApi.getAll();
      setUserData(apiData.data);
    } catch (e) {
      console.log("catching req error", e);
    }
  }

  // delete user
  async function deleteUser(id) {
    try {
      await applicationApi.delete(id);
      message.success('User deleted.');
      getAllUsers()
    } catch (e) {
      console.log("catching req error", e);
    }
  }

  useEffect(() => {

    getAllUsers();

  }, [])

  // add user
  function addUser(e) {
    let tempUser = newUser
    tempUser[e.target.name] = e.target.value
    setNewUser(tempUser)
  }
  const columns = [
    {
      title: 'Id',
      dataIndex: 'id',
    },
    {
      title: 'Created At',
      dataIndex: 'createdAt',
    },
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'email',
      dataIndex: 'email',
    },
    {
      title: 'Resume Url',
      dataIndex: 'resumeUrl',
    }, {
      title: 'Action',
      dataIndex: 'id',
      render: (id) => (
        <Popconfirm title="Sure to delete?"
          onConfirm={() => deleteUser(id)}>
          <a>Delete</a>
        </Popconfirm>
      ),
    }
  ];

  // post 
  async function onSubmit() {
    let body = newUser
    try {
      await applicationApi.create(body);
      message.success('User created.');
      setModelVisibility(false);
      getAllUsers();
      setNewUser({});
    } catch (e) {
      console.log("catching req error", e);

    }
  }

  return (
    <Row>
      <Col span={12} offset={6}>
        <h1 className="sub-head">List of User</h1>
        <div className="add-user-btn">
          <Button type="primary" onClick={() => setModelVisibility(true)}>Add User</Button>
        </div>
        <Row>
          <Col span={24}>
            <Table columns={columns} dataSource={userData} size="small" pagination={false} />
          </Col>
        </Row>
        <Modal title="Add User" visible={isModalVisible}
          onCancel={() => setModelVisibility(false)}
          footer={[
            <Button onClick={() => setModelVisibility(false)}> Cancel </Button>,
            <Button type="primary" onClick={() => onSubmit()}>Submit</Button>
          ]}
        >
          <span>Name</span>
          <Input name="name" onChange={(e) => addUser(e)} />
          <span>Email</span>
          <Input name="email" onChange={(e) => addUser(e)} />
          <span>Resume Url</span>
          <Input name="resumeUrl" onChange={(e) => addUser(e)} />
        </Modal>
      </Col>
    </Row>
  );
}

export default App;
