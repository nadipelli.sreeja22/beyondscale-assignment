import server from '../utils/server.js';

const prefixPath = `/application`;
const httpMethods = ['GET', 'POST', 'DELETE'];

const applicationApi = {
    getAll: async () => { 
        return await server.execute(prefixPath, httpMethods[0]) 
    },
    create: async (data) => { 
        return await server.execute(prefixPath, httpMethods[1], data) 
    },
    delete: async (id) => { 
        return await server.execute(`${prefixPath}/${id}`, httpMethods[2]) 
    }
};

export default applicationApi;